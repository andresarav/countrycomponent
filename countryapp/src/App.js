import React, { Component } from 'react';
import logo from './logo.svg';
import SelectCountryLanding from './components/selectCountriesLanding'
import FaqSection from './components/faqs/faqSection'
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <SelectCountryLanding></SelectCountryLanding>
        <FaqSection landing={true}/>
      </div>
    );
  }
}

export default App;
